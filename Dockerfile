FROM ubuntu:16.04
MAINTAINER "Jake Melin" <jmelin@riverainc.com>

RUN apt-get update
RUN apt-get install -y software-properties-common nano git ssh

# Add php repo
RUN add-apt-repository ppa:ondrej/php; exit 0
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 4F4EA0AAE5267A6C
RUN apt-get -y update
RUN apt-get install -y php5.6 php5.6-mcrypt php5.6-fpm php5.6-mbstring php5.6-curl php5.6-cli php5.6-mysql php5.6-gd php5.6-intl php5.6-xsl php5.6-zip

# Install and configure Nginx
RUN apt-get install -y nginx
# This needs to be site-available config change not the config below
ADD configs/sites-available.conf /etc/nginx/sites-available/default
ADD configs/nginx.conf /etc/nginx/nginx.conf

ADD configs/php.ini /etc/php/5.6/fpm/php.ini

ADD front /var/www/html
RUN rm /var/www/html/index.nginx-debian.html

RUN mkdir /var/www/html/files
RUN chown -R www-data /var/www/html/files
RUN chmod -R 755 /var/www/html/files

ADD configs/start.sh /home/start.sh
RUN chmod +x /home/start.sh

RUN apt-get remove apache2* -y

EXPOSE 80 443

CMD /home/start.sh && /bin/bash
