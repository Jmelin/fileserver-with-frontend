<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>Select Path | File Server</title>

</head>
<body>

  <div style="padding:20px;">
    <img src="images/eagle6-black.png" width="240px">
    <br>

    <?php
    if (empty($_POST) == false) {
      $target = $_POST['target'];

      header('Location: selectfile.php?target='.$target.'');
      exit();
    }
    ?>

    <h4>Please select the folder you want to upload to:<h4>

    <form action="" method="post">
      <select name='target' style='width:650px;'>
        <option value='choose'>Choose location</option>
        <option value='choose'>-----------</option>
        <?php
          $path = 'files/';
          // an unsorted array of dirs & files
          $files_dirs = iterator_to_array( new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path),RecursiveIteratorIterator::SELF_FIRST) );

          // create a new associative array with dirs as keys and their files
          $dirs_files = array();
          foreach($files_dirs as $dir) {
            if(is_dir($dir) AND preg_match('/\/\.$/',$dir)) {
              $d = preg_replace('/\/\.$/','',$dir);
              $dirs_files[$d] = array();
                foreach($files_dirs as $file) {
                   if(is_file($file) AND $d == dirname($file)) {
                     $f = basename($file);
                     $dirs_files[$d][] = $f;
                   }
                }
             }
          }
          // sort dirs
          ksort($dirs_files);

          foreach($dirs_files as $dir => $files) {
            if ($dir != 'files') {
              echo "<option value='$dir'>$dir</option>";
            }
          }
        ?>
      </select>
      <input type="submit" value="Next" name="submit">
    </form>

  </div>

  <div style="position: absolute; bottom:5px; right:10px; color: #111;">
    <?php $url="http://".$_SERVER['HTTP_HOST']."/"; echo $url; ?> | Copyright <?php echo date('Y') ?> AJ.
  </div>

</body>
</html>
