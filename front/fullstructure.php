<?php

$path = '/var/www/html/files';
// an unsorted array of dirs & files
$files_dirs = iterator_to_array( new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path),RecursiveIteratorIterator::SELF_FIRST) );

echo '<html><body><pre>';
// create a new associative array with dirs as keys and their files
$dirs_files = array();
foreach($files_dirs as $dir) {
   if(is_dir($dir) AND preg_match('/\/\.$/',$dir)) {
     $d = preg_replace('/\/\.$/','',$dir);
     $dirs_files[$d] = array();
      foreach($files_dirs as $file) {
         if(is_file($file) AND $d == dirname($file)) {
            $f = basename($file);
            $dirs_files[$d][] = $f;
         }
      }
   }
}
//print_r($dirs_files);

// sort dirs
ksort($dirs_files);

foreach($dirs_files as $dir => $files) {
  $c = substr_count($dir,'/');
  echo  str_pad(' ',$c,' ', STR_PAD_LEFT)."$dir\n";
  // sort files
  asort($files);
   foreach($files as $file) {
     echo str_pad(' ',$c,' ', STR_PAD_LEFT)."|_$file\n";
   }
}
echo '</pre></body></html>';

?>
