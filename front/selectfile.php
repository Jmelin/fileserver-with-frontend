<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>Upload File | File Server</title>

</head>
<body>
  <div style="padding:20px;">
    <img src="images/eagle6-black.png" width="240px">
    <br>

    <?php
    $target_dir = $_GET['target'];

      if (isset($_GET['file'])) {
          echo '
            <h4>Please select the file you want to upload:</h4>
              <p>You are uploading this file to: <h5>/'.$target_dir.'</h5></p>

              <form action="functions/upload-file.php?target='.$target_dir.'" method="post" enctype="multipart/form-data">
                  Select file to upload:<br>
                  <input type="file" name="fileToUpload" id="fileToUpload">
                  <input type="submit" value="Upload file" name="submit">
              </form>

            <h5>(Upload limit is 5GB)</h5>
          ';

      } else {
          echo '
            <h4>How do you want to upload?</h4>
            <a href="selectfile.php?file&target='.$target_dir.'">Upload File from Computer</a> | <a href="functions/upload-url.php?target='.$target_dir.'">Upload from URL</a>
          ';
      }
    ?>

  </div>

  <div style="position: absolute; bottom:5px; right:10px; color: #111;">
    <?php $url="http://".$_SERVER['HTTP_HOST']."/"; echo $url; ?> | Copyright <?php echo date('Y') ?> AJ.
  </div>

</body>
</html>
