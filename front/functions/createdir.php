<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>Create New Directory | File Server</title>

</head>
<body>

  <div style="padding:20px;">
    <img src="../images/eagle6-black.png" width="240px">
    <br>

    <?php
    if (empty($_POST) == false) {
      $new_directory = preg_replace('/\s+/', '', $_POST['newname']);

      mkdir("../files/$new_directory", 0755, true);

      header('Location: /files/');
      exit();
    }
    ?>

    <h4>Please enter the new directory name:<h4>

    <form action="" method="post">
      <input type="text" name="newname" style="width:250px;">
      <input type="submit" value="Create" name="submit">
    </form>

    <h5>To create nested directories: (ex. dir1/dir2/dir3)</h5>
  </div>

  <div style="position: absolute; bottom:5px; right:10px; color: #111;">
    <?php $url="http://".$_SERVER['HTTP_HOST']."/"; echo $url; ?> | Copyright <?php echo date('Y') ?> AJ.
  </div>

</body>
</html>
