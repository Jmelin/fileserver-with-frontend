<html>
<script>
  function preventBack(){window.history.forward();}
  setTimeout("preventBack()", 0);
  window.onunload=function(){null};
</script>

<?php
$target_dir = $_GET['target'];

echo "<p>You are uploading this file to: <h5>/$target_dir</h5></p>";
?>

<p>Please enter a publicly available URL:</p>
<form method="post">
<input name="url" size="150" />
<input name="submit" value="Download" type="submit" />
</form>
<p style='color:red;'>(The page will refresh when it is finished downloading.. Please don't leave the page..)</p>
<?php
    // maximum execution time in seconds
      set_time_limit (24 * 60 * 60);

      if (!isset($_POST['submit'])) die();

      // folder to save downloaded files to. must end with slash
      $destination_folder = '/var/www/html/'.$target_dir.'/';

      $url = $_POST['url'];
      $newfname = $destination_folder . basename($url);

      $file = fopen ($url, "rb");
      if ($file) {
        $newf = fopen ($newfname, "wb");

        if ($newf)
        while(!feof($file)) {
          fwrite($newf, fread($file, 1024 * 8 ), 1024 * 8 );
        }
      }

      if ($file) {
        fclose($file);
      }

      if ($newf) {
        fclose($newf);
        header('Location: /'.$target_dir.'');
      }
?>
</html>
