<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>Main | File Server</title>

</head>
<body>

  <div style="padding:20px;">
    <a href=""><img src="images/eagle6-black.png" width="240px"></a>
    <br>

    <h4>What do you want to do?</h4>

    <a href="selectpath.php">Upload New File</a> | <a href="functions/createdir.php">Make New Directory</a> | <a href="/files">View All Files</a>
  </div>

  <div style="position: absolute; bottom:5px; right:10px; color: #111;">
    <a href="fullstructure.php">...</a> | <?php $url="http://".$_SERVER['HTTP_HOST']."/"; echo $url; ?> | Copyright <?php echo date('Y') ?> AJ.
  </div>

</body>
</html>
