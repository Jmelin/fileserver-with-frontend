Docker Container - Ubuntu OS - Launches a frontend to Upload files and create New Directories

Includes Dockerfile and docker-compose.yml file.

Project Started 1/6/2017


To-do's
1. Make directories function  -- DONE
2. List directories at the top level  -- DONE
3. Upload file function (5 GB limit)  -- DONE
4. Recursively search directory structure   -- DONE
5. Upload from public URL  -- DONE
6. Move content function
7. Stylize the tool
8. Make a CentOS base container
